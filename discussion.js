// CRUD Operations

// Insert Documents (CREATE)
/*

	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})

		Insert Many Document
			db.collectionName.insertMany([
				{
				"fieldA": "valueA",
				"fieldB": "valueB"
			},
			{
				"fieldA": "valueA",
				"fieldB": "valueB"
			}

			])
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
})

db.users.insertMany([
		{
			"firstName": "Stephen",
			"lastName": "Hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "none"
		},
		{
			"firstName": "Neil",
			"lastName": "Armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "none"
		}
	])


	//Mini Activity
		/*
			1. Make a new collection with the name "courses"
			2. Insert the following fields and values

				name: Javascript 101
				price: 5000
				description: Introduction to Javascript
				isActive: true

				name: HTML 101
				price: 2000
				description: Introduction to HTML
				isActive: true

				name: CSS 101
				price: 2500
				description: Introduction to CSS
				isActive: false
		*/

db.courses.insertMany([
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
		},
		{
			"name": "HTML 101",
			"price": 2000,
			"description": "Introduction to HTML",
			"isActive": true
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false
		}
	])

//Find Documents (Read)
/*
	Syntax:
		db.collectionName.find() - this will retrieve all our documents
		db.collectionName.find({"criteria": "value"}) - this will retrieve all our documents that will match with our criteria

		db.collectionName.fineOne({}) will return the first document in our collection

		db.collectionName.findOne({"criteria": "value"}) - will return the first document in our collection that will match our criteria

*/

db.users.find();

db.users.find({
	"firstName": "Jane"
})

db.users.findOne()

db.users.findOne({
	"department": "none"
})

// Updating documents (update)
/*
	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		}),
		{
			$set: {
				"fieldToBeUpdate": "updatedValue"
			}
		}

		db.collectionName.updateMany({
			"criteria": "value"
		}),
		{
			$set: {
				"fieldToBeUpdate": "updatedValue"
			}
		}


*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "Test@mail.com",
	"department": "none"
})

// Updating One Document
db.users.updateOne(
	{
		"firstName": "Test"

	},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations",
			"status": "active"
		}
	}
)

db.users.updateMany(
	{
		"department": "none"

	},
	{
		$set: {
			"department": "HR",
		}
	}
)

// Removing a filed
db.users.updateOne({
	"firstName": "Bill"
	}, 
	{
		$unset: {
			"status": "active"
		}

});

// Rename
db.users.updateMany(
		{},
		{
			$rename: {
				"department": "dept"
			}
		}
);

/*
	Mini Activity:
		1. In our courses collection, update the HTML 101 course
			-Make the isActive to false
		2. Add enrollees fiels to all the documents in our courses
			-Enrollees: 10

*/

db.courses.updateOne({
	"name": "HTML 101"
},
{
	$set: {
		"isActive": false
	}
}
)

db.courses.updateMany(
	{},
	{
		$set: {
			"Enrollees": 10
		}
	}
)

// Deleting Documents (Delete)

/*
	Syntax:
		-db.collectionName.deleteOne({"criteria": "value"})
		-db.collectionName.deleteMany({"criteria": "value"})
*/

db.users.insertOne({
	"firstName": "Test"
});

db.users.deleteOne({"firstName": "Test"});

db.users.deleteMany({"dept": "HR"});

db.courses.deleteMany({});


